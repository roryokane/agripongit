/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

import { GIT_INIT, GIT_COMMIT, GIT_BRANCH, GIT_CHECKOUT, GIT_MERGE, GIT_REMOTE_ADD, GIT_PUSH, GIT_FETCH } from '../actions/trees';
import Immutable from 'immutable';
import sha1 from 'sha-1';

const generateCommitHash = (commits) => {
  return sha1(commits.size.toString());
}

const trees = (state = {}, action) => {
  if(typeof(state.local) === 'undefined'){
    if(action.type === GIT_INIT){
      return Object.assign({}, state, {
        local: Immutable.Map({})
      });
    }
    return state;
  }
  // At this point, we can assume a git repo has been initialised

  switch(action.type){
    case GIT_COMMIT:
      const key = generateCommitHash(state.local);
      const commit = {
        message: 'henk',
        parents: (state.head) ? [state.head]: [],
        children: []
      };
      if(typeof state.head === 'undefined'){
        // This is the first commit
        return Object.assign({}, state, {
          local: Immutable.Map({[key]: commit}),
          root: key,
          head: key,
          branches: Immutable.Map({ master: key }),
          currentBranch: 'master'
        });
      } else {
        const currentParent = state.local.get(state.head);
        const parent = Object.assign({}, currentParent, {
          children: [...currentParent.children, key]
        });
        return Object.assign({}, state, {
          local: state.local.set(key, commit).set(state.head, parent),
          head: key,
          branches: state.branches.set(state.currentBranch, key)
        });
      }
      break;
    case GIT_BRANCH:
      return Object.assign({}, state, {
        branches: state.branches.set(action.newBranchName, state.head)
      });
    case GIT_CHECKOUT:
      if(state.branches.has(action.commit)){
        return Object.assign({}, state, {
          head: state.branches.get(action.commit),
          currentBranch: action.commit
        });
      }
      return state;
    case GIT_MERGE:
      if(state.branches.has(action.branch)){
        const key = generateCommitHash(state.local);
        const secondParentHash = state.branches.get(action.branch);
        const commit = {
          message: `merge ${action.branch} into ${state.currentBranch}`,
          parents: [state.head, secondParentHash],
          children: []
        };
        const currentParent1 = state.local.get(state.head);
        const parent1 = Object.assign({}, currentParent1, {
          children: [...currentParent1.children, key]
        });
        const currentParent2 = state.local.get(secondParentHash);
        const parent2 = Object.assign({}, currentParent2, {
          children: [...currentParent2.children, key]
        });
        return Object.assign({}, state, {
          local: state.local.set(key, commit).set(state.head, parent1).set(secondParentHash, parent2),
          head: key,
          branches: state.branches.set(state.currentBranch, key)
        });
      }
      return state;
    case GIT_REMOTE_ADD:
      // We don't actually do anything here yet; later, we might add matching local branches with
      // remote ones with different names
      return state;
    case GIT_PUSH:
      return Object.assign({}, state, {
        branches: state.branches.set(`${action.remote}/${action.branch}`, state.branches.get(action.branch))
      });
    case GIT_FETCH:
      // We're cheating a little bit here: we pretend that there has been a new commit on the given
      // remote every time a new fetch is performed. This avoids us having to actually keep track
      // of a remote and the changes happening on it.
      const remoteCommitHash = generateCommitHash(state.local);
      const branchName = `${action.remote}/${state.currentBranch}`;
      const parentHash = state.branches.get(branchName);
      const remoteCommit = {
        message: `commit on remote ${action.remote}/${state.currentBranch}`,
        parents: [parentHash],
        children: []
      };
      const parent = state.local.get(parentHash);
      const updatedParent = Object.assign({}, parent, {
        children: [...parent.children, remoteCommitHash]
      });
      return Object.assign({}, state, {
        local: state.local.set(remoteCommitHash, remoteCommit).set(parentHash, updatedParent),
        branches: state.branches.set(branchName, remoteCommitHash)
      });
    default:
      return state;
  }
};

export default trees;
