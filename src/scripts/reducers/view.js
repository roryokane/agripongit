/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

import { VISIT_SECTION } from '../actions/view';
import Immutable from 'immutable';

const view = (state = { visited: { }}, action) => {
  switch(action.type){
    case VISIT_SECTION:
      return Object.assign({}, state, {
        currentSection: action.sectionName,
        visited: Object.assign({}, state.visited, { [action.sectionName]: true })
      });
    default:
      return state;
  }
};

export default view;
