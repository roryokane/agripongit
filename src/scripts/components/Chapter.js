/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

import React from 'react';
import ReactDOM from 'react-dom'
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';

let scrollHandlerRunning = false;

const Chapter = React.createClass({
  mixins: [ReactPureRenderMixin],
  propTypes: {
    onEnter: React.PropTypes.func,
    sections: React.PropTypes.arrayOf(React.PropTypes.shape({
      sectionName: React.PropTypes.string.isRequired,
      actions: React.PropTypes.arrayOf(React.PropTypes.func).isRequired
    })),
    visitedSections: React.PropTypes.object.isRequired
  },
  getClass: function(sectionName){
    return (this.props.visitedSections[sectionName] ? ' is-visited' : '') +
           (this.props.currentSection === sectionName ? ' is-current' : '');
  },
  onVisit: function(sectionName){
    if(typeof(this.props.onVisit) !== 'undefined'){
      return this.props.onVisit(sectionName);
    }
  },
  handleScroll(event){
    if(scrollHandlerRunning){
      return;
    }
    scrollHandlerRunning = true;
    // Find the first section that hasn't been marked visited, if any,
    // that is at or above the current scroll position
    const currentSection = this.props.sections
    .find(({sectionName}) => {
      const rect = ReactDOM.findDOMNode(this.refs[sectionName]).getBoundingClientRect();
      // The offset of the current section's bottom edge to the top of the viewport is:
      //     - higher than zero (otherwise it's above the current viewport), and
      return rect.bottom > 0 &&
      //     - lower than its height (otherwise there's another element above it that's still in
      //       the viewport)
             rect.bottom <= rect.height;
    });

    if(typeof(currentSection) === 'undefined'){
      scrollHandlerRunning = false;
      return;
    }

    // If this is the first time the user has scrolled to this section,
    // dispatch its associated actions
    if(!this.props.visitedSections[currentSection.sectionName]){
      currentSection.actions.forEach((dispatchAction) => dispatchAction());
    }

    // Now mark it as visited
    this.onVisit(currentSection.sectionName);
    scrollHandlerRunning = false;
  },
  componentDidMount(){
    if (typeof window !== 'undefined') {
      document.addEventListener('scroll', this.handleScroll);
    }
  },
  componentWillUnmount(){
    if (typeof window !== 'undefined') {
      document.removeEventListener('scroll', this.handleScroll);
    }
  },
  render: function() {
    const sections = this.props.sections.map((section) => {
      const capitalisedSectionName = section.sectionName.charAt(0).toLocaleUpperCase() + section.sectionName.slice(1);
      const sectionElement = require(`./sections/${capitalisedSectionName}`);
      const element = React.createElement(sectionElement);
      return <section className={this.getClass(section.sectionName)} name={section.sectionName} ref={section.sectionName} key={section.sectionName}>
        {element}
      </section>;
    });
    return <div className="chapter">
      {sections}
    </div>;
  }
});

export default Chapter;
