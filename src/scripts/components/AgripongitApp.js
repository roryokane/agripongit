/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

import Immutable from 'immutable';

let initialState = {};

if (typeof window !== 'undefined') {
  // Things we need when running inside the browser

  require("babel/polyfill");

  if(window.__INITIAL_STATE__){
    // The page was pre-rendered during the build, providing an initial state on the client side
    initialState = window.__INITIAL_STATE__;
    // Unfortunately, Immutable collections had to be transformed into regular JS objects,
    // so we need to convert them back.
    initialState.snapshots = Immutable.Map(initialState.snapshots).map(snapshot => {
      snapshot.local = Immutable.Map(snapshot.local);
      snapshot.branches = Immutable.Map(snapshot.branches);
      return snapshot;
    });
  }

  // CSS
  require('../../styles/normalize.css');
  require('../../styles/main.scss');
  require('../../styles/_javascriptEnabled.scss');
}

var React = require('react');
var ReactDOM = require('react-dom');
var ReactPureRenderMixin = require('react-addons-pure-render-mixin');
var Repository = require('./Repository');

import { createStore, bindActionCreators } from 'redux';
import { Provider, connect } from 'react-redux';

import agripongitStore from '../reducers/agripongit';
const store = createStore(agripongitStore, initialState);

import { visitSection } from '../actions/view';
const onVisit = bindActionCreators(visitSection, store.dispatch);

import Chapter from './Chapter';

// Map subsets of the store to properties
const select = (state) => {
  return { trees: state.trees, view: state.view };
};

import chapter1Factory from '../chapters/chapter1';
const chapter1 = chapter1Factory(store.dispatch);

var AgripongitApp = connect(select)(React.createClass({
  mixins: [ReactPureRenderMixin],
  renderVisualisation: function(){
    if(!this.props.trees.root){
      return <div>
        <div className="repository">Empty repository</div>
      </div>;
    }
    return <div className="containsSvg">
      <div className="repository hasCommits">Empty repository</div>
      <svg
       className="file"
       version="1.1"
       width="100%"
       height="100%"
       viewBox="0 0 100 100"
       preserveAspectRatio="xMidYMid slice"
       id="file1">
       <defs>
        <radialGradient id="circleFill" cx="0.52" cy="0.48">
          <stop offset="0%"/>
          <stop offset="95%"/>
          <stop offset="100%"/>
        </radialGradient>
         <linearGradient id="borderFill" x1="0" y1="1" x2="1" y2="0">
           <stop offset="75%"/>
           <stop offset="95%"/>
           <stop offset="100%"/>
         </linearGradient>
       </defs>
       <g>
         <Repository tree={this.props.trees.local}
                     root={this.props.trees.root}
                     head={this.props.trees.head}
                     branches={this.props.trees.branches}
                     currentBranch={this.props.trees.currentBranch} />
       </g>
      </svg>
    </div>;
  },
  render: function() {
    return (
      <article className="agripongit">
        <div className="mobileWarning">
          Please note: Due to the nature of this tutorial it looks best on reasonably wide screens.
        </div>
        <div className="text">
          <header className="intro">
            <h1><img src="images/logo.svg" className="logo" />A Grip On Git<br/>
              <small>A simple, visual Git tutorial</small></h1>
            <p className="stats">Reading time ~11 minutes | By <a href="//VincentTunru.com">Vincent Tunru</a></p>
          </header>
          <section className="intro">
            <p>Have you <a href="https://xkcd.com/1597/" title="xkcd on Git">memorised a few Git commands</a>, without actually understanding what's going on? Then you've come to the right place! This how-to should help you to get a better grip on what is arguably one of the most important tools in software development at the moment.</p>
            <p>This interactive tutorial visualises what is happening when you're using Git. As you scroll down the page, you will be guided through the most important basic concepts of Git, while applying them to an example repository visualised on the right.</p>
            {
              // <p>Note that the purpose of this tutorial is not to teach the basic commands, nor does it focus on teaching best practices. It also does not try to give <a href="https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain" target="_blank" title="An actual detailed overview of Git's internals">a detailed overview of the inner workings of Git</a>: some of the implementation details are skipped or simplified to make it easier to understand.</p>
              }
          </section>
          <Chapter onVisit={onVisit}
                   sections={chapter1}
                   visitedSections={this.props.view.visited}
                   currentSection={this.props.view.currentSection} />
          <section className="outro">
            <p>That wraps up this Git tutorial. I hope it helped you grasp the fundamental concepts, so that you won't feel completely lost when you have to perform more advanced operations in Git. If you're interested, do check out <a href="https://gitlab.com/Vinnl/agripongit" title="A grip on Git">the source code</a> of this tutorial (naturally, in a Git repository), and let me know what you think.</p>
          </section>
          <footer className="outro backMatter">
            <p>By <a href="http://VincentTunru.com" title="My website">Vincent Tunru</a></p>
          </footer>
        </div>
        <div className={(Object.keys(this.props.view.visited).length > 0) ? 'visualisation started' : 'visualisation'}>
          <div className="javascriptDisabled">Javascript is disabled. While you can read the content, the accompanying visualisation that would appear here needs Javascript to work.</div>
          { this.renderVisualisation() }
        </div>
      </article>
    );
  }
}));

// Only render directlly when inside the browser
if (typeof window !== 'undefined') {
  ReactDOM.render(
    <Provider store={store}>
      <AgripongitApp />
    </Provider>, document.getElementById('content')); // jshint ignore:line
}

module.exports = AgripongitApp;
