/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var CommitToNewBranch = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>As said, a new commit will be the child of the commit <code>HEAD</code> is currently pointing to.</p>
      <Highlight className="bash">git commit</Highlight>
      <p>You can now see why they were called <em>branches</em>: with <code>myBranch</code> pointing to a different commit than <code>master</code>, our simple timeline has suddenly evolved into a tree structure.</p>
    </div>;
  }
});

module.exports = CommitToNewBranch;
