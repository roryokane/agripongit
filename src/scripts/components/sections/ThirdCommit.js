/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var ThirdCommit = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <Highlight className="bash">git commit</Highlight>
      <p>The new commit is based on the commit previously labeled <code>HEAD</code>, and the label is then updated to refer to the new commit as <code>HEAD</code>.</p>
      <p>Now let's turn to the label dubbed <code>master</code>. This is called a <em>branch</em>. Like <code>HEAD</code>, branches point to a commit. Unlike <code>HEAD</code>, you can name them yourself. Furthermore, you can have multiple branches. As you can see, the default first branch is called <code>master</code>.</p>
    </div>;
  }
});

module.exports = ThirdCommit;
