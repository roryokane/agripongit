/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var AddRemote = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>The features we've touched upon up to now are already incredibly powerful in allowing you to work on multiple versions of the same project. They enable you to confidently make sweeping changes to your project without fear of losing anything. After all, if you're unsatisfied with your changes, you can always start anew from a commit that does not include those changes.</p>
      <p>However, Git has more tricks up its sleeve. A lot more, in fact, but for now we will only focus on how it enables effective collaboration.</p>
      <p>As we have seen, you have your own repository, consisting of the project files and a tree representing their history. The same holds true of other team members: each has a separate repository, including the project files in a certain state. None of these are special, however, a team usually decides <em>by convention</em> to have a single central repository that contains the code that will be distributed to the user later on. Much like the <code>master</code> branch should have the latest version of those parts of the project you've completed, the central repository should include the latest completed work by every team member. Let's look at how we can get our completed work into the central repository.</p>
      <p>Let's assume that our editor has initialised an empty Git repository at <code>https://publisher.com/book.git</code>. Our first step is that we need to make our local repository aware of this other repository:</p>
      <Highlight className="bash">git remote add publisher https://publisher.com/book.git</Highlight>
      <p>From our local repository's point of view, the other repositories are called <em>remotes</em>. Each remote has a name; we called the one we just added <code>publisher</code>.</p>
    </div>;
  }
});

module.exports = AddRemote;
