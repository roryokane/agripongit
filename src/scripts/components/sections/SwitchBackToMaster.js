/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var SwitchBackToMaster = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>However, if we switch to that branch now:</p>
      <Highlight className="bash">git checkout master</Highlight>
      <p>…the chapter we wrote in <code>myBranch</code> is gone. The reason for this is, of course, that while we have merged <em><code>master</code></em> to <em><code>myBranch</code></em>, we haven't merged <em><code>myBranch</code></em> to <em><code>master</code></em> yet. This might feel cumbersome, but by first copying everything into <code>myBranch</code>, we had the opportunity to check whether everything still looked spiffy after the merge. Since we now know that it did, we can merge it back to <code>master</code> without risking it resulting in a book with a messed-up layout.</p>
    </div>;
  }
});

module.exports = SwitchBackToMaster;
