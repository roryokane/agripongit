/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var SecondCommit = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>We stage it using <code>git add outline.txt</code>, then create another commit:</p>
      <Highlight className="bash">git commit</Highlight>
      <p>As you can see, our new commit is connected to the previous one (its <em>parent</em>). You can think of a commit as containg a list of the changes needed to turn the files as they were at the previous commit into the way they are now.</p>
      <p>Now consider the label reading <code>HEAD</code> next to our newly created commit. <code>HEAD</code> can be seen as a pointer: when a new commit is made, it will be a child of the commit <code>HEAD</code> is pointing to.</p>
      <p>As an example, let's say we spelled someone's name wrong. After correcting it, we <code>git add thanks.txt</code> again, and create a new commit:</p>
    </div>;
  }
});

module.exports = SecondCommit;
