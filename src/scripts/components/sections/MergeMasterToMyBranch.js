/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var MergeMasterToMyBranch = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>Of course Git can help us here: we can <em>merge</em> the state of the files at <code>master</code>'s commit with the state of the files in the current branch's latest commit:</p>
      <Highlight className="bash">git merge master</Highlight>
      <p>As you can see, Git has created a new <em>merge commit</em> for us in <code>myBranch</code>. As opposed to regular commits, merge commits have multiple parents. This means that it contains multiple sets of changes: for each parent, the changes needed to make to it to make it incorporate the changes in the other parent(s).</p>
      <p>Now is a good time to check whether both sets of changes work well together. In the case of our book, we could check whether our edits to our earlier chapters didn't mess up the layout of our new one.</p>
      <p>If everything looks allright, <code>myBranch</code> now contains the latest complete version of our book. But now imagine that we'd have another branch with another half-finished chapter. If we'd merge <code>myBranch</code> into this other branch when that chapter is finished, and into another one for another chapter, and so on, then after a while we would lose track of which branch contains the latest version. It is therefore common practice to keep the latest complete version of a project in the <code>master</code> branch.</p>
    </div>;
  }
});

module.exports = MergeMasterToMyBranch;
