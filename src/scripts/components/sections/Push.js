/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var Push = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>In this example, there's no code in the remote yet. Let's change that by <em>pushing</em> our master branch to <code>publisher</code>.</p>
      <Highlight className="bash">git push publisher master</Highlight>
      <p>Several things have now happened. First, all the commits needed to go from an empty repository to the state of our local repository at <code>master</code> have been sent to the <code>publisher</code> remote. Secondly, we have a local reference to the currently known state of the remote's <code>master</code> branch as <code>publisher/master</code>. Finally, our local <code>master</code> branch is <em>matched</em> to <code>publisher/master</code>, roughly meaning that Git knows they are related.</p>
    </div>;
  }
});

module.exports = Push;
