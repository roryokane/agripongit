/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var FirstCommit = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>This is what Git's history looks like after our first</p>
      <Highlight className="bash">git commit</Highlight>
      <p>The circle represents this first commit — please disregard the labels next to it for now.</p>
      <p>If, later on in our project, we revert back to this commit, all we would be left with is <code>thanks.txt</code> in its current state.</p>
      <p>So why did we not include <code>outline.txt</code> in our first commit? Basically, a good rule-of-thumb is that a commit should only contain changes that can be undone together. For example, if we later want to overhaul the structure of our book, we might want to ditch the outline we just wrote without also throwing away or list of people to thank.</p>
      <p>Therefore, let us tell Git about <code>outline.txt</code> in a separate commit.</p>
    </div>;
  }
});

module.exports = FirstCommit;
