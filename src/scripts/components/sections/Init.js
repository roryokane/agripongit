/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
import ReactPureRenderMixin from 'react-addons-pure-render-mixin';
import Highlight from 'react-highlight';

var Init = React.createClass({
  mixins: [ReactPureRenderMixin],
  render: function() {
    return <div>
      <p>Imagine we're starting a new project that we want to manage using Git. This could be a lot of things, but for the sake of this tutorial, let's say we're writing a book. The first order of action is to make Git aware of it. Inside the folder that is to house our book we initialise a new Git repository:</p>
      <Highlight className="bash">git init</Highlight>
      <p>Git has created a new hidden folder called <code>.git</code> — everything that Git knows about our book will live in there, <em>apart</em> from our actual project.</p>
      <p>Now let's say that we've already created two files: <code>thanks.txt</code>, in which we will keep track of who we need to include in the word of thanks, and <code>outline.txt</code>, which contains a general outline of what our book will be about.</p>
      <p>Let's give Git an actual version to control — it is, after all, a <i>version control system</i>. You have to make a conscious choice about what each version (or in Git speak: <em>commit</em>) includes; for now, we only <code>git add thanks.txt</code>. This is called <em>staging</em> the file <code>thanks.txt</code>: marking it to be included in the next commit.</p>
    </div>;
  }
});

module.exports = Init;
