/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

// Action types
export const VISIT_SECTION = 'VISIT_SECTION';

// Action creators
export const visitSection = (sectionName) => ({ type: VISIT_SECTION, sectionName: sectionName });
