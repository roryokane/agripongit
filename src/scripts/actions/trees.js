/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

// Action types
export const GIT_INIT = 'GIT_INIT';
export const GIT_COMMIT = 'GIT_COMMIT';
export const GIT_BRANCH = 'GIT_BRANCH';
export const GIT_CHECKOUT = 'GIT_CHECKOUT';
export const GIT_MERGE = 'GIT_MERGE';
export const GIT_REMOTE_ADD = 'GIT_REMOTE_ADD';
export const GIT_PUSH = 'GIT_PUSH';
export const GIT_FETCH = 'GIT_FETCH';

// Action creators
export const gitInit = () => ({ type: GIT_INIT });
export const gitCommit = () => ({ type: GIT_COMMIT });
export const gitBranch = (newBranchName) => ({ type: GIT_BRANCH, newBranchName: newBranchName });
export const gitCheckout = (commit) => ({ type: GIT_CHECKOUT, commit: commit });
export const gitMerge = (branch) => ({ type: GIT_MERGE, branch: branch });
export const gitRemoteAdd = (shortName) => ({ type: GIT_REMOTE_ADD, shortName: shortName });
export const gitPush = (remote, branch) => ({ type: GIT_PUSH, remote: remote, branch: branch });
export const gitFetch = (remote) => ({ type: GIT_FETCH, remote: remote });
