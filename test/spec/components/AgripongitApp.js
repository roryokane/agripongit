'use strict';

describe('Main', function () {
  var React = require('react');
  var AgripongitApp, component;

  beforeEach(function () {
    var container = document.createElement('div');
    container.id = 'content';
    document.body.appendChild(container);

    AgripongitApp = require('../../../src/scripts/components/AgripongitApp.js');
    component = React.createElement(AgripongitApp);
  });

  it('should create a new instance of AgripongitApp', function () {
    expect(component).toBeDefined();
  });
});
