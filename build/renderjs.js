/*
A Grip on Git - A simple, visual Git tutorial
Copyright (C) 2016  Vincent Tunru

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

'use strict';

var React = require('react');
var ReactDOMServer = require('react-dom/server');
var Provider = require('react-redux').Provider;
var createStore = require('redux').createStore;

var AgripongitApp = require('../src/scripts/components/AgripongitApp.js');
var agripongitStore = require('../src/scripts/reducers/agripongit');
var store = createStore(agripongitStore);

var chapter1Factory = require('../src/scripts/chapters/chapter1');
var visitSection = require('../src/scripts/actions/view').visitSection;

function reduceToInitialState(state){
  // It would be better to fetch the initial state from the reducers themselves
  // (except for the snapshots reducer), in case it's changed there.
  return {
    snapshots: state.snapshots,
    trees: {},
    view: { visited: {} }
  };
}

module.exports = function(grunt){
  grunt.registerTask('renderjs', function(){
    var index = grunt.file.read(grunt.config.get('pkg.dist') + '/index.html');


    var appComponent = React.createElement(AgripongitApp);
    var providerComponent = React.createElement(Provider, {store: store}, appComponent);

    // Pre-compute possible states for each section by pretending to visit them
    var chapter1 = chapter1Factory(store.dispatch);
    chapter1.forEach(function(section){
      section.actions.forEach(function(dispatchAction){
        dispatchAction();
      });
      store.dispatch(visitSection(section.sectionName));
    });

    // For the initial state, we only need the snapshots.
    // Thus, replace the reducers with one that only preserves the snapshots,
    // then fire an arbitrary action to trigger it.
    store.replaceReducer(reduceToInitialState);
    store.dispatch({ type: 'arbitraryAction' });
    var stateToInject = store.getState();
    stateToInject.snapshots = stateToInject.snapshots.toJS();
    stateToInject = 'window.__INITIAL_STATE__ = ' + JSON.stringify(stateToInject) + ';';

    // Replace everything inside `<!-- build:renderjs` and `endbuild -->`
    // We need `[^]` instead of `.` because the latter doesn't match newlines
    index = index.replace(/\s*<!-- build:renderjs([^]*?)endbuild -->\s*/, ReactDOMServer.renderToString(providerComponent));
    index = index.replace('window.__INITIAL_STATE__ = {};', stateToInject);

    grunt.file.write(grunt.config.get('pkg.dist') + '/index.html', index);
  });
};
